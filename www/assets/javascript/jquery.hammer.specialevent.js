(function($, Hammer) {
    'use strict';

    // no jQuery!
    if(typeof $ === "undefined") {
        return;
    }

    $.each(
        [
        'tap','doubletap','hold','rotate',
        'swipe','swipeup','swipedown','swipeleft','swiperight',
        'pinch','pinchin','pinchout',
        'transformstart','transform','transformend',
        'dragstart','drag','dragend',
        'pinch','pinchin','pinchout',
        'touch','release'
        ],
        function(i, event) {
            $.event.special[event] = {
                setup: function(data, namespaces, eventHandler) {
                    var $target = $(this);
                    if(!$target.data('hammer')) {
                        $target.data('hammer', new Hammer(this, data));
                    }

                    var hammer = $target.data('hammer');

                    hammer.on(event,function (ev) {
                        $target.trigger($.Event(event, ev));
                    });
                },

                teardown: function(namespaces) {
                    var $target = $(this),
                        hammer = $target.data('hammer');

                    if(hammer && hammer['on'+ event]) {
                        delete hammer['on'+event];
                    }
                }
            }
        }
    );
    

})(window.jQuery, window.Hammer);
