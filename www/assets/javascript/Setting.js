var Setting = (function(){
	var notification = function(){
		var prop = {
			alert:true,
			confirm:true,
			beep:true,
			vibrate:false,
			msg:false
		};
		notification.prototype.model=function(){
			return Backbone.Model.extend({
				defaults:prop
			});
		};
		_.extend(this,prop);
	};
	return {
		'notification':new notification()
	};
})();