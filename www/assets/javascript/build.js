var build = (function(window){
    var PLATFORM = {
    	'Web':'1',
    	'iOS':'2',
    	'Android':'3',
    	'iOS-iPhone':'21',
    	'iOS-iPod':'22',
    	'Android':'31',
    	'Web-Chrome':'11'
    },
    CURRENT = PLATFORM['iOS-iPod'],
    platform = function(PLATFORM,CURRENT){
    	this['CURRENT'] = CURRENT;
	    platform.prototype.is = function(dis) {
			return this['CURRENT'] && (this['CURRENT'].indexOf(PLATFORM[dis])==0 || this['CURRENT'] === PLATFORM[dis]);
	    };
    };
	return {
		'PLATFORM':PLATFORM,
		'platform':new platform(PLATFORM,CURRENT)
	};
})(window);