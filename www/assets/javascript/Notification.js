var notification = {
	settings:Setting.notification,
	sounds:{
		finished:"/assets/ogg/GameOverGuitar.mp3"
	},
	msg:{
		url:"http://weichuan.me/api/web/transmit",
		id:{
			ucode:963977,
			type:20,
			content:''
		}
	},
	init:function(){
		this.load_settings();
	},
	load_settings:function(){

	},
	model:function(){

	},
	alert:function(msg,callback,type){
		var self = this;
		self.sendMsg(msg);
		var audio = this.sound(type);
		audio.play();
		setTimeout(function(){
			alert(msg);
			audio.pause();
			if(callback){
				callback.call(self,audio);
			}
		},300);
		return audio;
	},
	confirm:function(msg,yes,no,type){
		var self = this;
		self.sendMsg(msg);
		var audio = this.sound(type);
		audio.play();
		var doyes = function(){
			audio.pause();
			if(yes){
				yes();
			}
		};
		var dono = function(){
			audio.pause();
			if(no){
				no();
			}
		};
		setTimeout(function(){
			confirm(msg)?doyes():dono();
		},300);

	},
	sound:function(type){
        type = type || 'finished';
		if(this.settings.beep){
			var audio = null;
			if(build['platform'].is('iOS')){
				audio = new Media(this.sounds[type]);
			}else if(build['platform'].is('Web')){
				audio = new Audio(this.sounds[type]);
				audio.loop = true;
				audio.volume = 0.5;
			}
			return audio;
		}
	},
	sendMsg:function(msg){
		if(this.settings.msg){
			var data = _.clone(this.msg.id);
			data.content = msg;
			$.post(this.msg.url,data);
		}
	}
};