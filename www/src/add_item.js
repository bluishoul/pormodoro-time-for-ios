var AddTemplate = [
	'<header class="bar-title">',
	  '<a class="button-prev" href="javascript:void();" id="pt_list">列表</a>',
	  '<h1 class="title">增加活动</h1>',
	  '<a class="button" href="javascript:void();" id="pt_add_now">确认</a>',
	'</header>',
	'<div class="content">',
		'<form id="add_item_form">',
			'<input type="hidden" name="create_time"/>',
			'<div class="input-group">',
				'<div class="input-row">',
					'<label>名称</label>',
					'<input type="text" name="name" placeholder="任务名称">',
				'</div>',
				'<div class="input-row">',
					'<div class="btn-input">',
					  	'<label>番茄数</label>',
					  	'<div class="button dec-btn">-</div><input data-max="9" class="pt-btn-input" type="text" name="count" maxlength="1" value="1"/ ><div class="button inc-btn">+</div>',
					'</div>',
				'</div>',
				'<div class="input-row">',
					'<div class="btn-input">',
					  	'<label>时长(分)</label>',
					  	'<div class="button dec-btn">-</div><input data-max="60" class="pt-btn-input" type="text" name="duration" maxlength="2" value="25"/ ><div class="button inc-btn">+</div>',
					'</div>',
				'</div>',
				'<div class="input-row">',
					'<label>最后期限</label>',
					'<input type="text" name="dead_line" placeholder="点击选取时间" readonly="true">',
				'</div>',
				'<div class="input-row">',
					'<label>备注</label>',
					'<textarea rows="5" name="remarks"></textarea>',
				'</div>',
			'</div>',
			'<a class="button-positive button-block" id="add-item-btn">马上添加</a>',
			'<a class="button-negative button-block" id="clear-btn">清空</a>',
		'</form>',
	'</div>'
].join("\n");

var AddView = Jr.View.extend({
	render:function(){
		this.$el.html(AddTemplate);
		return this;
	},
	events:{
		"click .dec-btn,.inc-btn":"onClickBtnInput",
		"blur .pt-btn-input":"onBlurBtnInput",
		"click #add-item-btn,#pt_add_now":"onClickAddItemBtn",
		"click #clear-btn":"onClickClearBtn",
		"click #pt_list":"onClickListButton"
	},
	//转换视图到“list”
	onClickListButton:function(e){
		global.goto_list_view();
		return false;
	},
	onClickBtnInput:function(event){
		var target = $(event.srcElement);
		var input = target.siblings("input");
		var maxValue = input.data("max");
		var value = +input.val();
		if(target.attr("class").indexOf("dec-btn")!=-1){
			value = value <= 1 ? 1 : value-1;
		}else{
			value = value >= maxValue ? maxValue : value+1;
		}
		input.val(value);
	},
	onBlurBtnInput:function(event){
		var target = $(event.srcElement);
		var maxValue = target.data("max");
		var value = +target.val();
		value = value <= 1 ? 1 : value;
		value = value >= maxValue ? maxValue : value;
		target.val(value);
	},
	onClickAddItemBtn:function(){
		var form = $("#add_item_form");
		if(this.check_add_form(form)){
			var item = new TaskItem(global.auto_initialize(form,TaskItem,{
				"create_time":(new Date().getTime())
			}));
			global.storage().save("task",item,false,function(){
				global.goto_list_view();
			});
		}
	},
	onClickClearBtn:function(){
		if(!confirm("确认清空？"))
			return;
		var form = $("#add_item_form");
		form.find('[name="name"]').val("");
		form.find('[name="dead_line"]').val("");
		form.find('[name="remarks"]').val("");
		form.find('[name="count"]').val(1);
		form.find('[name="duration"]').val(25);
	},
	check_add_form:function(form){
		var name = form.find('[name="name"]');
		if(_.string.trim(name.val()).length==0){
			alert("任务名称不能为空！");
			name.focus();
			return false;
		}
		return true;
	}
});