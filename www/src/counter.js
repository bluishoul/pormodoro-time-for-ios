var CounterTemplate = [
	'<header class="bar-title">',
	  '<a class="button-prev" href="javascript:void();" id="pt_list">列表</a>',
	  '<h1 class="title"><span id="task_name"></span></h1>',
	'</header>',
	'<div class="content">',
		'<div class="task-progress">',
			'<div class="task-progress-bar">',
				'<div class="task-progress-finished"></div>',
			'</div>',
			'<div class="task-progress-word">0/0</div>',
		'</div>',
		'<div class="task-timer">',
			'<div class="task-timer-progress"></div>',
		'</div>',
		'<div id="play_button">',
				'<a class="butt">PAUSE</a>',
		'</div>',
	'</div>'
].join("\n");

var CounterView = Jr.View.extend({
	render:function(){
		this.$el.html(CounterTemplate);
		this.initCounter();
		return this;
	},
	events:{
		"click #pt_list":"onClickListButton",
		"click #pt_add_now":"onClickAddNowButton"
	},
	initCounter:function(){
		var self = this;
		global.storage().get("task",+self.id,function(rows){
			if(rows.length==0){
				global.goto_list_view();
			}
			self.model = rows[0];
			self.update_progress_bar(self.model);
			$("#task_name").html(self.model.name);
			self.start_timer();
		});
	},
	start_timer:function(){
		var self = this;
		var total_time = 60 * self.model.duration;
    	var loader = self.loader = $(".task-timer-progress").percentageLoader({
    		height:250,
    		width:250
    	});
		self.update_timer(total_time,total_time,loader);
		loader.setProgress(1);
		var count = 0;
		self.data = {
			count:0,
			total:total_time
		};
		var itv = self.itv = self.setup_interval(loader);
		$("#play_button").toggle(function(){
			$(this).find(".butt").html("PLAY");
			self.clear_interval(true);
		},function(){
			$(this).find(".butt").html("PAUSE");
			itv = self.setup_interval(loader);
		});
	},
	start_next_time:function(){
		var self = this;
		var model2 = _.clone(self.model);
		var self = this;
		var count = +model2.count;
		var finished = +model2.finished + 1;
		model = new TaskItem(model2);
		model.set("finished",finished);
		global.storage().save("task",model,true,function(){
			self.update_progress_bar(model);
			self.clear_interval();
			var goon = function(){
				self.setup_interval();
				self.model = self.jsonOfModel(model);
				self.data = {
					total:self.data.total,
					count:0
				}
			};
			var stopnow = function(){
				self.clear_interval();
				global.goto_list_view();
			};
			if(finished < count){
				goon();
			}else{
				notification.confirm("已完成全部 "+count+" 个番茄时间，返回列表？",stopnow,goon);
			}
		})
	},
	update_timer:function(time,total,loader){
		var minutes = Math.floor(time/60);
		var seconds = ("0")+time % 60;
		seconds = seconds.substr(seconds.length-2,seconds.length);
		loader.setValue(minutes+':'+ seconds);
		loader.setProgress(time/total);
	},
	setup_interval:function(loader){
		var self = this;
		if(!loader)
			loader = self.loader;
		return self.itv = setInterval(function(){
			var total_time = self.data.total,
				count = self.data.count;
			count++;
			self.data = {
				total:total_time,
				count:count
			}
			if(count >= total_time){
				self.start_next_time();
				count=0;
			}
			var left = total_time-count;
			self.update_timer(left,total_time,loader);
		},1000);
	},
	clear_interval:function(pause){
		clearInterval(this.itv);
		if(!pause){
				this.data = {
				total:this.data.total,
				count:0
			}
		}
	},
	update_progress_bar:function(model){
		//设置条形进度条
		var finished = model.finished;
		var count = model.count;
		if(typeof finished != "number"){
			finished = model.get("finished");
			count = model.get("count");
		}
		var prg = $(".task-progress-finished");
		prg.css({
			width:(finished>=count?100:(finished*100/count))+"%"
		});

		$(".task-progress-word").html(finished+"/"+count);
	},
	//转换视图到“list”
	onClickListButton:function(e){
		this.clear_interval();		
		global.goto_list_view();
		return false;
	},
	//确认添加
	onClickAddNowButton:function(e){
		
	},
	jsonOfModel:function(model){
		return model.attributes?model.attributes:model;
	}
});