var TaskMenuView = Jr.View.extend({
	tagName:'div',
	events:{
		'tap li':'onMenuItemTap'
	},
	onMenuItemTap:function(e){
		var self = this;
		var parent = this.options.parent;
		var cur = $(e.srcElement);
		var action = cur.data("action");
		var action_list = {
			"play":function(id){
				global.goto_counter_view(id.id);
			},
			"remove":function(id){
				if(confirm("确定要删除该任务？")){
					parent.model.remove(function(){
						
					});
				}
			}
		}
		action = action_list[action];
		var id = parent.$el.data("id");
		if(action){
			action.call(this,{
				id:id
			});
		}
	},
	render:function(){
		var menu_tpl = [
	    	'<ul class="clearfix">',
	    		'<li data-action="remove">',
	    			'<span class="icon-remove"></span>',
	    		'</li>',
	    		'<li>',
    				'<span class="icon-question-sign"></span>',
	    		'</li>',
	    		'<li data-action="play">',
					'<span class="icon-play-circle"></span>',
	    		'</li>',
	    		'<li>',
	    			'<span class="icon-question-sign"></span>',
	    		'</li>',
	    		'<li>',
    				'<span class="icon-question-sign"></span>',
    			'</li>',
	    	'</ul>'
		].join('\n');
		$(this.el).addClass("task_menu").html(menu_tpl);
		return this;
	}
});

var TaskItemView = Jr.View.extend({
	tagName:'li',
	menu:null,
	events:{
		'tap':'onTap'
	},
	onTap:function(){
		var menu = this.menu.$el.toggle();
		$('.task_menu').not(menu).hide();
	},
	render:function(){
		var item = this.model;
		if (!item)return;
		var finished = item.get("count")<=item.get("finished");
		var count_class = finished?"count-positive":"count";
		var task_class = finished?"task-finished":"";
		var progress = item.get("finished") + "/" + item.get("count");
		var itemTpl = [
		    '<a class="' + task_class + ' item_name" href="javascript:void();">',
		      item.get("name"),
		      '<span class="' + count_class + '">' + progress + '</span>',
		    '</a>'
		].join("\n");
		var menu = new TaskMenuView({
			parent:this
		});
		this.menu = menu;
		$(this.el).html(itemTpl).attr("data-id",item.id).append(menu.render().el);
		return this;
	}
});

var ListTemplate = [
	'<header class="bar-title">',
	  //'<a class="button" href="javascript:void();" id="pt_menu">菜单</a>',
	  '<h1 class="title">活动列表</h1>',
	  '<a class="button-next" href="javascript:void();" id="pt_add_task">添加</a>',
	'</header>',
	'<div class="content">',
		'<div id="empty_words">尚未添加任何任务，点击右上角“添加”任务</div>',
		'<ul class="list" id="task_list">',
		'</ul>',
	'</div>'
].join('\n');

var ListView = Jr.View.extend({
	initialize:function(){
		this.load_items();
	},
	render_:function(){
		var self = this;
		this.$el.html(ListTemplate);
		_(this.collection.models).each(function(item){
			self.appendTaskItem(item);
		},this);
		if(this.collection.models.length>0){
			$("#empty_words").hide();
		}else{
			$("#empty_words").show();
		}
		return this;
	},
	events:{
		"click #pt_add_task":"onClickAddTaskButton"
	},
	//转换视图到add
	onClickAddTaskButton:function(e){
		global.goto_add_view();
		return false;
	},
	appendTaskItem:function(item){
		var self = this;
		var itemView = new TaskItemView({
			model:item
		});
		$("#task_list").append(itemView.render().el);
	},
	load_items:function(collection){
		var self = this;
		$("#task_list").html("");
		this.collection = new TaskList();
		this.collection.bind("add",self.appendTaskItem);
		this.collection.on("remove",function(){
			self.render_();
		});
		global.storage().query("SELECT t.* FROM (SELECT *,(finished*100/count) as c FROM task) as t ORDER BY t.c ASC , t.create_time DESC",[],function(models){
			_.each(models,function(model,key){
				self.collection.add(new TaskItem(model));
			});
			self.render_();
		});
	}
});
