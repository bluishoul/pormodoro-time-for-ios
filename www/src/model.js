var TaskItem = Backbone.Model.extend({
	defaults:{
		id:0,
		name:"",
		count:0,
		finished:0,
		duration:25,
		dead_line:"",
		remarks:"",
		create_time:""
	},
	remove:function(callback){
		this.collection.remove(this);
		global.storage().remove("task",this.id,callback);
	}
});

var TaskList = Backbone.Collection.extend({
	model:TaskItem
});