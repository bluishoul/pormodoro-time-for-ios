var global = {
	goto_view:function(name,dir){
		Jr.Navigator.navigate(name,{
			trigger:true,
			animation:{
				type:Jr.Navigator.animations.SLIDE_STACK,
				direction:dir
			}
		});
	},
	goto_list_view:function(){
		this.goto_view('',Jr.Navigator.directions.RIGHT);
	},
	goto_add_view:function(){
		this.goto_view('add',Jr.Navigator.directions.LEFT);

	},
	goto_counter_view:function(id){
		this.goto_view('counter/'+id,Jr.Navigator.directions.LEFT);
	},
	storage:function(){
		if(!this.stg){
			this.stg = storage.init({
				db_name:"pomodoro-time",
				models:{
					'task':TaskItem
				},
				index:{
					'task':['finished']
				}
			}).create_tables().create_index();
		}
		return this.stg;
	},
	auto_initialize:function(form,Model,default_){
		var keys = [];
		var values = [];
		var model = {};
		_.each(this.storage()._get_model_attr(Model),function(value,key){
			keys.push(key);
			var v = form.find('[name="'+key+'"]');
			if(v.length!=0){
				var vl = v.val();
				var type = typeof value;
				model[key] = type=="string"?vl:type=="number"?parseInt(vl):vl;
			}else{
				model[key] = value;
			}
		});
		return _.extend(model,default_);
	}
};


var AppRouter = Jr.Router.extend({
	routes:{
		'':'list',
		'add':'add',
		'counter/:id':'counter'
	},

	list:function(){
		var listView = new ListView();
		this.renderView(listView);
	},

	add:function(){
		var addView = new AddView();
		this.renderView(addView);
	},

	counter:function(id){
		var counterView = new CounterView({id:id});
		this.renderView(counterView);
	}
});

var appRouter = new AppRouter();
Backbone.history.start();